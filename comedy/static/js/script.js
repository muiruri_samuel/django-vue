window.onload = function () {
	var app = new Vue({
	  delimiters: ['[[', ']]'],
	  el: '#app',
	  data: {
	    jokes: [],

	    joke_id: 1,
	    dadjokes_id: 1,
	    tweet_id: 1,

	    first_active: true,
	    second_active: false,
	    third_active: false,

	    checked: 'Joke',
	  },
	  created: function() {
	  	this.postdata(initial_data = {'id': 1, 'model-name': 'Joke'})

	  	var self = this;
     
	    setInterval(function(){ //get new content after 30 seconds 
	        self.refresh() 
	    }, 30000);
	  },
	  methods: {
		postdata: function (retrieve_data = []){
			initial_data = {'id': 1, 'model-name': 'Joke'}

			if (typeof retrieve_data == "object") {
				initial_data = retrieve_data
			}

		    fetch("\start-jokes\/", {
		        body: JSON.stringify(initial_data),
		        cache: 'no-cache', 
		        credentials: 'same-origin', 
		        headers: {
			        'user-agent': 'Mozilla/4.0 MDN Example',
			        'content-type': 'application/json'
		        },
		        method: 'POST',
		        mode: 'cors', 
		        redirect: 'follow',
		        referrer: 'no-referrer',
		        })
		        .then(response => response.json()).then((json) => {
		          	this.jokes.unshift(...json['jokes'])

		          	if (initial_data['model-name'] == 'Joke') {
		          		this.joke_id = json['ref-id']
		          	} else if (initial_data['model-name'] == 'DadJokes') {
		          		this.dadjokes_id = json['ref-id']
		          	} else if(initial_data['model-name'] == 'Tweet') {
		          		this.tweet_id = json['ref-id']
		          	}
		    	})
		},
		refresh: function() {
			var id;
			
			if (this.checked == 'Joke') {
				id = this.joke_id
			} else if (this.checked == 'DadJokes') {
				id = this.dadjokes_id
			} else if (this.checked == 'Tweet') {
				id = this.tweet_id
			}

			var initial_data = {'model-name': this.checked, 'id': id};
			this.postdata(initial_data)
		}
	}
	});
};

	